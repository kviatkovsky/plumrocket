<?php

namespace Plumrocket\Callforprice\Block;

use Magento\Customer\Model\Session;

class Product extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct( \Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Framework\App\Request\Http $request,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
                                \Magento\Customer\Model\SessionFactory $customerSession
    )
    {
        $this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    public function getUser()
    {
        $customer = $this->_customerSession->create();
        return $customer->getCustomer()->getData();
    }
}
