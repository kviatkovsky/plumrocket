<?php
namespace Plumrocket\Callforprice\Model\ResourceModel\Order;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'plumrocket_callforprice_post_collection';
    protected $_eventObject = 'order_collection';


    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Plumrocket\\Callforprice\\Model\\Order')
    {
        $this->_init('Plumrocket\Callforprice\Model\Order', 'Plumrocket\Callforprice\Model\ResourceModel\Order');
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return \Magento\Review\Model\ResourceModel\Review\Product\Collection
     */
    public function create(array $data = array())
    {
        return $this->_objectManager->create($this->_instanceName, $data);
    }

}