<?php

namespace Plumrocket\Callforprice\Model;

use Plumrocket\Callforprice\Model\ResourceModel\Order\Collection;

class Order extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'plumrocket_callforprice_post';

    protected $_cacheTag = 'plumrocket_callforprice_post';

    protected $_eventPrefix = 'plumrocket_callforprice_post';

    protected function _construct()
    {

        $this->_init('Plumrocket\Callforprice\Model\ResourceModel\Order');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function addFieldToFilter($attribute, $condition = null)
    {
            return parent::addFieldToFilter($attribute, $condition);
    }
}