<?php
namespace Plumrocket\Callforprice\Controller\Adminhtml\CallForPrice;

use Magento\Framework\Controller\ResultFactory;

class Delete extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $record_id = $this->getRequest()->getParam('id');
        $order = $this->_objectManager->create('Plumrocket\Callforprice\Model\Order');
        $order->load($record_id);
        $order->delete();

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }


}
