<?php

namespace Plumrocket\Callforprice\Controller\Index;


use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute()
    {
        $user_name = $this->getRequest()->getParam('user_name');
        $product_id = $this->getRequest()->getParam('product_id');
        $email_address = $this->getRequest()->getParam('email_address');
        $phone = $this->getRequest()->getParam('phone');
        $message = $this->getRequest()->getParam('message');

        $order = $this->_objectManager->create('Plumrocket\Callforprice\Model\Order');
        $order->setData('Rule Name', $user_name);
        $order->setData('Email', $email_address);
        $order->setData('Phone', $phone);
        $order->setData('Description', $message);
        $order->setData('Product_id', $product_id);
        $order->setData('Priority', 'new');
        $order->save();

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
