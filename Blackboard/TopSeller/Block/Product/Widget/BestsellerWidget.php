<?php

namespace Blackboard\TopSeller\Block\Product\Widget;
/**
 * Best Seller products widget
 */
class BestsellerWidget extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_template = 'bestsellerproduct.phtml';

    /**
     * Default value for products count that will be shown
     */
    const DEFAULT_PRODUCTS_COUNT = 10;
    const DEFAULT_IMAGE_WIDTH = 250;
    const DEFAULT_IMAGE_HEIGHT = 250;
    /**
     * Products count
     *
     * @var int
     */
    protected $_productsCount;
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;
    protected $_resourceFactory;
    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    /**
     * Product collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * Image helper
     *
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;
    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $_cartHelper;

    protected $_productloader;
    /**
     * BestsellerWidget constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $resourceFactory
     * @param \Magento\Reports\Model\Grouped\CollectionFactory $collectionFactory
     * @param \Magento\Reports\Helper\Data $reportsData
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $resourceFactory,
        \Magento\Reports\Model\Grouped\CollectionFactory $collectionFactory,
        \Magento\Reports\Helper\Data $reportsData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        array $data = []
    )
    {
        $this->_resourceFactory = $resourceFactory;
        $this->_productloader = $_productloader;
        $this->_collectionFactory = $collectionFactory;
        $this->_reportsData = $reportsData;
        $this->_imageHelper = $context->getImageHelper();
        $this->_cartHelper = $context->getCartHelper();
        $this->_scopeConfig = $scopeConfig;
        $this->getTopSellerTitle();
        parent::__construct($context, $data);
    }

    /**
     * Image helper Object
     */
    public function imageHelperObj()
    {
        return $this->_imageHelper;
    }

    /**
     * get featured product collection
     */
    public function getProduct($productId)
    {
        $limit = $this->getProductLimit();

        $resourceCollection = $this->_resourceFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection');
        $resourceCollection->setPageSize($limit);

            $currentProduct = $this->_productloader->create()->load($productId);

        return $currentProduct;
    }

    /**
     * get featured product collection
     */
    public function getBestsellerProduct()
    {
        $limit = $this->getProductLimit();

        $resourceCollection = $this->_resourceFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection');
        $resourceCollection->setPageSize($limit);


        foreach ($resourceCollection as $item)
        {
            $currentProduct = $this->_productloader->create()->load($item->getProductId());

            $bestSellerProductsArray[] = [
                'productId' =>  $item->getProductId(),
                'productName' =>  $currentProduct->getName(),
                'productURL' =>  $currentProduct->getProductUrl(),
                'productImage' =>  $this->getUrl('pub/media/catalog').'product'.$currentProduct->getImage(),
                'productSalable' =>  $currentProduct->getIsSalable(),

            ];
//            var_dump($bestSellerProductsArray);
//            die;
        }
//        var_dump($bestSellerProductsArray);
//        die;
        return $bestSellerProductsArray;
    }

    /**
     * Get the configured limit of products
     * @return int
     */
    public function getProductLimit()
    {
        if ($this->getData('productcount') == '') {
            return self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->getData('productcount');
    }

    /**
     * Get the widht of product image
     * @return int
     */
    public function getProductimagewidth()
    {
        if ($this->getData('imagewidth') == '') {
            return self::DEFAULT_IMAGE_WIDTH;
        }
        return $this->getData('imagewidth');
    }

    /**
     * Get the height of product image
     * @return int
     */
    public function getProductimageheight()
    {
        if ($this->getData('imageheight') == '') {
            return self::DEFAULT_IMAGE_HEIGHT;
        }
        return $this->getData('imageheight');
    }

    /**
     * Get the add to cart url
     * @return string
     */
    public function getAddToCartUrl($productID, $additional = [])
    {
        $product = $this->_productloader->create()->load($productID);
        return $this->_cartHelper->getAddUrl($product, $additional);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param null $priceType
     * @param string $renderZone
     * @param array $arguments
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null,
        $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
        array $arguments = []
    )
    {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }
        $arguments['zone'] = isset($arguments['zone'])
            ? $arguments['zone']
            : $renderZone;
        $arguments['price_id'] = isset($arguments['price_id'])
            ? $arguments['price_id']
            : 'old-price-' . $product->getId() . '-' . $priceType;
        $arguments['include_container'] = isset($arguments['include_container'])
            ? $arguments['include_container']
            : true;
        $arguments['display_minimal_price'] = isset($arguments['display_minimal_price'])
            ? $arguments['display_minimal_price']
            : true;
        /** @var \Magento\Framework\Pricing\Render $priceRender */
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');
        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                $arguments
            );
        }

        return $price;
    }

    public function getTopSellerTitle()
    {
        $topSellerTitle = $this->_scopeConfig->getValue('topSellerTitle/parameters/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return ($topSellerTitle) ? $topSellerTitle : 'Top Selling items 1111';
    }
}